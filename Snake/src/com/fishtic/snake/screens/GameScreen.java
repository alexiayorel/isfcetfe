package com.fishtic.snake.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.fishtic.snake.SnakeGame;
import com.fishtic.snake.controller.WorldController;
import com.fishtic.snake.model.World;
import com.fishtic.snake.view.WorldRenderer;

public class GameScreen implements Screen, InputProcessor {

	private Game game;
	private World world;
	private WorldRenderer worldRenderer;
	private WorldController worldController;

	public GameScreen(Game game) {
		this.game = game;
	}

	@Override
	public void render(float delta) {
		worldController.update(delta);
		worldRenderer.render();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		this.world = new World();
		this.worldRenderer = new WorldRenderer(world);
		this.worldController = new WorldController(world);
		
		Gdx.input.setInputProcessor(this);
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.LEFT)
			worldController.leftPressed();
		if (keycode == Keys.RIGHT)
			worldController.rightPressed();
		if (keycode == Keys.DOWN)
			worldController.downPressed();
		if (keycode == Keys.UP)
			worldController.upPressed();
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
