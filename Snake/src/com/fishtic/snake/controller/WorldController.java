package com.fishtic.snake.controller;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.fishtic.snake.model.Fruit;
import com.fishtic.snake.model.World;
import com.fishtic.snake.model.snake.PathCoord;
import com.fishtic.snake.model.snake.Snake;
import com.fishtic.snake.model.snake.SnakeP.Direction;
import com.fishtic.snake.view.WorldRenderer;

public class WorldController {
	
	private enum State {
		IDLE, RUNNING, OVER, NEW
	}
	
	private enum Keys {
		LEFT, RIGHT, UP, DOWN, NO
	}
	
	private Keys keys;
	private State state;
	private World world;
	private Snake snake;
	private Fruit fruit;
	private float timeDelta;
	private float totalTime;
	
	private Array<PathCoord> path;
	private boolean keyLock;
	
	

	
	public WorldController(World world) {
		this.world = world;		
		state = State.NEW;
		keys = Keys.NO;
		keyLock = false;

	}
	
	public void update(float delta) {
		if(state == State.NEW) {
			world.genWorld();
			snake = world.getSnake();
			
			fruit = world.getFruit();
			path = snake.getPath();
			state = State.IDLE;
		
		}

		if(state == State.RUNNING) {
			timeDelta += delta;
			if(readyToUpdate()) {
				
				
				world.clearLastPos();
							
				switch(keys) {
				case DOWN:
					snake.setDirection(Direction.DOWN);
					keys = Keys.NO;
					break;
				case LEFT:
					snake.setDirection(Direction.LEFT);
					keys = Keys.NO;
					break;
				case RIGHT:
					snake.setDirection(Direction.RIGHT);
					keys = Keys.NO;
					break;
				case UP:
					snake.setDirection(Direction.UP);
					keys = Keys.NO;
					break;
				default:
					break;
				
				}
						
				snake.moveSnake(1);
				outBoundsAlt();
				
				if(fruitCollision()) {
					world.addScore(100);
					outBoundsAlt();
				}
				
				if(snakeCollision()) {
					state = State.OVER;
				}
 				
 				if(state == State.RUNNING) {
 	 				world.updateFruitGrid();
 	 				world.updateSnakeGrid();
 				}
 				
				timeDelta = 0;
				keyLock = false;
				
				
			}
			
		}
		


	}
	
	public boolean readyToUpdate() {
		if(timeDelta > (0.01 * snake.getSpeed())) {
			return true;
		}
		return false;
	}

	
	public boolean outBounds() {
		if(snake.getSnakeV().first().getX() > World.GRID_SIZE || snake.getSnakeV().first().getY() > World.GRID_SIZE || snake.getSnakeV().first().getX() < 0 || snake.getSnakeV().first().getY() < 0) {
			
			Gdx.app.log("game over", "sortie grille");
			return true;
		}
		return false;
	}
	
	public void outBoundsAlt() {
		
		for(int i = 0; i < snake.getSnakeV().size ; i++) {
			if(snake.getSnakeV().get(i).getX() < 0) {
				snake.getSnakeV().get(i).setX(World.GRID_SIZE - 1);
			}
			if(snake.getSnakeV().get(i).getX() > World.GRID_SIZE - 1) {
				snake.getSnakeV().get(i).setX(0);
			}
			if(snake.getSnakeV().get(i).getY() > World.GRID_SIZE - 1) {
				snake.getSnakeV().get(i).setY(0);
			}
			if(snake.getSnakeV().get(i).getY() < 0) {
				snake.getSnakeV().get(i).setY(World.GRID_SIZE - 1);
			}
			
		}
	}
	
	
	public boolean snakeCollision() {
		for(int i = 1; i < snake.getSnakeV().size ; i++ ) {
			if(snake.getSnakeV().first().getX() == snake.getSnakeV().get(i).getX() && snake.getSnakeV().first().getY() == snake.getSnakeV().get(i).getY() ) {

				Gdx.app.log("score", "" + world.getScore());
				return true;
			}
		}
		return false;
	}
	
	public boolean fruitCollision() {
		if(fruit.getX() == snake.getSnakeV().first().getX() && fruit.getY() == snake.getSnakeV().first().getY()) {
			
			world.fruitGen();
			snake.addPart(2);
			outBoundsAlt();
			snake.addSpeed(0.1f);
			return true;

		}
		return false;
	}
	

	
	
	public void upPressed() {
		if(state == State.IDLE) {
			state = State.RUNNING;
		}
		if(!keyLock && keys != Keys.UP && keys != Keys.DOWN) {
			keyLock = true;
			keys = Keys.UP;
		}
			
	}

	public void downPressed() {
		if(state == State.IDLE) {
			state = State.RUNNING;
		}
		if(!keyLock && keys != Keys.DOWN && keys != Keys.UP) {
			keyLock = true;
			keys = Keys.DOWN;
		}
			
	}

	public void rightPressed() {
		if(state == State.IDLE) {
			state = State.RUNNING;
		}
		if(!keyLock && keys != Keys.RIGHT && keys != Keys.LEFT) {
			keyLock = true;
			keys = Keys.RIGHT;	
		}
					
	}	

	public void leftPressed() {
		if(state == State.IDLE) {
			state = State.RUNNING;
		}
		if(!keyLock && keys != Keys.LEFT && keys != Keys.RIGHT) {
			keyLock = true;
			keys = Keys.LEFT;
		}
			
	}

}
