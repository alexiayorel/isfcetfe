package com.fishtic.snake.model;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.fishtic.snake.model.snake.Snake;
import com.fishtic.snake.model.snake.SnakeP.Direction;



public class World {
	
	public final static int GRID_SIZE = 50;
	public final static float SPEED_START = 10f; 
	
	private int grid[][];
	private Snake snake;
	private Fruit fruit;
	private int score;
	

	
	public World() {
		grid = new int [GRID_SIZE][GRID_SIZE];	
	}

	public void genWorld() {
		snake = new Snake(GRID_SIZE/2, GRID_SIZE/2, Direction.RIGHT, SPEED_START);
		
		
		snake.addPart(2);
		updateSnakeGrid();
		fruit = new Fruit();
		fruitGen();
		updateFruitGrid();
		
		score = 0;
	}
	
	public void fruitGen() {
		int x,y;
		
		x = MathUtils.random(0, World.GRID_SIZE -1);
		y = MathUtils.random(0, World.GRID_SIZE -1);
		
		for(int i = 0; i < snake.getSnakeV().size; i++) {
			while (((x = MathUtils.random(0, World.GRID_SIZE -1)) == snake.getSnakeV().get(i).getX()) && ((y = MathUtils.random(0, World.GRID_SIZE -1)) == snake.getSnakeV().get(i).getY()));
		}
		
		fruit.setX(x);
		fruit.setY(y);
	}
	
	public void clearGrid() {
		for (int i=0; i<grid.length; i++) {
			for (int j=0; j<grid[i].length; j++) {
				grid[i][j] = 0;
			}
		}
	}
	
	public void clearLastPos() {
		grid[snake.getSnakeV().peek().getX()][snake.getSnakeV().peek().getY()] = 0;
	}
	
	public void updateFruitGrid() {
		if(!(fruit.getX() < 0 || fruit.getX() > GRID_SIZE -1 || fruit.getY() < 0 || fruit.getY() > GRID_SIZE -1))
			grid[fruit.getX()][fruit.getY()] = 3;
		else
			Gdx.app.log("debug", "erreur");
	}
	
	public void updateSnakeGrid() {
		
		grid[snake.getSnakeV().first().getX()][snake.getSnakeV().first().getY()] = 1;
		
		for(int i=1; i<snake.getSnakeV().size; i++) {
			//Gdx.app.log("debug", "" + snake.getSnakeV().get(i).getX() + " " + snake.getSnakeV().get(i).getY() );
			grid[snake.getSnakeV().get(i).getX()][snake.getSnakeV().get(i).getY()] = 2;
		}
	}
	
	public void updateSnakeGridAlt() {
		grid[snake.getSnakeV().first().getX()][snake.getSnakeV().first().getY()] = 1;
		
	}
	


	public void setScore(int score) {
		this.score = score;
	}

	//getters, setters
	public int[][] getGrid() {
		return grid;
	}

	public void setGrid(int[][] grid) {
		this.grid = grid;
	}

	public Snake getSnake() {
		return snake;
	}

	public void setSnake(Snake snake) {
		this.snake = snake;
	}
	public Fruit getFruit() {
		return fruit;
	}

	public void setFruit(Fruit fruit) {
		this.fruit = fruit;
	}

	public void addScore(int nb) {
		score = score * 2 + nb;
	}
	
	public int getScore() {
		return score;
	}
}
