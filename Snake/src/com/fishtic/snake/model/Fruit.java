package com.fishtic.snake.model;

import com.badlogic.gdx.math.MathUtils;

public class Fruit {
	
	private int x,y;
	
	public Fruit(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Fruit(int range) {
		newPos(range);
	}
	
	public Fruit() {
		
	}
	
	public void newPos(int range) {
		x = MathUtils.random(0, range);
		y = MathUtils.random(0, range);

	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
