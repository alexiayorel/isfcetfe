package com.fishtic.snake.model.snake;

import com.fishtic.snake.model.snake.SnakeP.Direction;


public class PathCoord {
	
	private int x, y;
	private Direction direction;
	
	public PathCoord(int x, int y, Direction direction) {
		this.x = x;
		this.y = y;
		this.direction = direction;
		
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}



}
