package com.fishtic.snake.model.snake;



public class SnakeP {
	
	public enum Direction {
		UP, DOWN, RIGHT, LEFT
	}
	
	private Direction direction;
	private int x,y;
	
	public SnakeP(int x, int y, Direction direction){
			
		this.direction = direction;
		this.x = x;
		this.y = y;
		
	}
	
	public void movePart(int nb) {

		switch(direction) {
		case DOWN:
			y -= nb;
			break;
		case LEFT:
			x -= nb;
			break;
		case RIGHT:
			x += nb;
			break;
		case UP:
			y += nb;
			break;
		default:
			break;
		
		}
		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

}
