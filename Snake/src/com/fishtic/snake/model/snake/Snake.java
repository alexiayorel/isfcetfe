package com.fishtic.snake.model.snake;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.fishtic.snake.model.World;
import com.fishtic.snake.model.snake.SnakeP.Direction;

public class Snake {
	
	private int x;
	private int y;
	private Array<SnakeP> snakeV;
	private Array<PathCoord> path;
	private Direction direction;
	private float speed;
	
	public Snake(int x, int y, Direction direction, float speed) {
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.speed = speed;
		path = new Array<PathCoord>();
		snakeV = new Array<SnakeP>();
		snakeV.add(new SnakeP(x, y, direction));
		
	}
	


	public void moveSnake(int nb) {
		for(int i=0; i<snakeV.size; i++) {		
			for(int j=0; j<path.size; j++) { // test path
				if((snakeV.size > 1) && (snakeV.get(i).getX() == path.get(j).getX() && snakeV.get(i).getY() == path.get(j).getY())) {
					snakeV.get(i).setDirection(path.get(j).getDirection());
					if(i+1 == snakeV.size) {
						Gdx.app.log("debug", "removeCoord" + path.first().getX() + " " + snakeV.first().getY());
						path.removeIndex(0);
					}
				}
				
			}
			snakeV.get(i).movePart(nb);	
		}	
			
	}
		
	public void addPart(int nb) {	
		for(int i = 0; i < nb ; i ++ ) {
			switch(snakeV.peek().getDirection()) {
			case DOWN:
				snakeV.add(new SnakeP(snakeV.peek().getX(), snakeV.peek().getY()+1, Direction.DOWN));
				break;
			case LEFT:
				snakeV.add(new SnakeP(snakeV.peek().getX()+1, snakeV.peek().getY(), Direction.LEFT));
				break;
			case RIGHT:
				snakeV.add(new SnakeP(snakeV.peek().getX()-1, snakeV.peek().getY(), Direction.RIGHT));
				break;
			case UP:
				snakeV.add(new SnakeP(snakeV.peek().getX(), snakeV.peek().getY()-1, Direction.UP));
				break;
			default:
				break;
			}
		}
		
	}
	
	public void addSpeed(float nb) {
		speed -= nb;
	}
	
	public Array<SnakeP> getSnakeV() {
		return snakeV;
	}

	public void setSnakeV(Array<SnakeP> snakeV) {
		this.snakeV = snakeV;
	}
	
	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
		this.snakeV.first().setDirection(direction);
		if(snakeV.size > 1) {
			path.add(new PathCoord(snakeV.first().getX(),snakeV.first().getY(), direction));
			Gdx.app.log("debug", "addCoord" + snakeV.first().getX() + " " + snakeV.first().getY());
		}
		
	}
	
	public Array<PathCoord> getPath() {
		return path;
	}

	public void setPath(Array<PathCoord> path) {
		this.path = path;
	}
	
	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

}
