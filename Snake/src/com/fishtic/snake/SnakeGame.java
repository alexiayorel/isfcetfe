package com.fishtic.snake;


import com.badlogic.gdx.Game;
import com.fishtic.snake.screens.GameScreen;


public class SnakeGame extends Game {

	@Override
	public void create() {
		setScreen(new GameScreen(this));
		
	}


}
