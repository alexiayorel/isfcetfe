package com.fishtic.snake.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import com.fishtic.snake.model.World;

public class WorldRenderer {
	
	public static boolean GRID_DEV = false;
	
	private World world;
	private OrthographicCamera cam;
	private ShapeRenderer debugRenderer;
	private float screenW, screenH;
	
	private int[][] grid;
	private int sizeCell;
	private int paddingCell;
	
	public WorldRenderer(World world) {
		this.world = world;
		grid = world.getGrid();
		screenW = Gdx.graphics.getWidth();
		screenH = Gdx.graphics.getHeight();
		sizeCell = (int) screenW / grid.length ;
		paddingCell = sizeCell / 5;
		
		cam = new OrthographicCamera();
		cam.setToOrtho(false, screenW, screenH);
		cam.update();
		
		debugRenderer = new ShapeRenderer();
		debugRenderer.setProjectionMatrix(cam.combined);
		
	}
	
	public void render() {
		Gdx.gl.glClearColor(0f, 0f, 0f, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		debugRenderer.begin(ShapeType.Rectangle);
		for (int i=0; i<grid.length; i++) {
			for (int j=0; j<grid[i].length; j++) {
				if(GRID_DEV) {
					debugRenderer.setColor(Color.GRAY);
					debugRenderer.rect(i * sizeCell , j * sizeCell, sizeCell, sizeCell );
				}

					
				switch (grid[i][j]) {
				case 1:
					debugRenderer.setColor(Color.RED);
					debugRenderer.rect(i * sizeCell + paddingCell , j * sizeCell + paddingCell, sizeCell - paddingCell * 2, sizeCell - paddingCell * 2 );
					break;
				case 2:
					debugRenderer.setColor(Color.WHITE);
					debugRenderer.rect(i * sizeCell + paddingCell , j * sizeCell + paddingCell, sizeCell - paddingCell * 2, sizeCell - paddingCell * 2 );
					break;
				case 3:		
					//debugRenderer.rect(i * sizeCell + paddingCell , j * sizeCell + paddingCell, sizeCell - paddingCell * 2, sizeCell - paddingCell * 2 );
					break;
				default:
					break;
				}		
			}
		}	
		debugRenderer.end();	
		debugRenderer.begin(ShapeType.Circle);
		for (int i=0; i<grid.length; i++) {
			for (int j=0; j<grid[i].length; j++) {
				switch (grid[i][j]) {
				case 3:
					debugRenderer.setColor(Color.YELLOW);
					debugRenderer.circle(i * sizeCell + sizeCell /2  , j * sizeCell + sizeCell /2, (sizeCell) /4 );
					break;
				}
			}
		}
		debugRenderer.end();
		
	}

}
